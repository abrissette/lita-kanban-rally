require "simplecov"
require "coveralls"
require "lita-kanban-rally"
require "lita/rspec"

Lita.version_3_compatibility_mode = false

SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter.new ([
  SimpleCov::Formatter::HTMLFormatter,
  Coveralls::SimpleCov::Formatter
])
SimpleCov.start { add_filter "/spec/" }

