require "spec_helper"

describe Lita::Handlers::KanbanRally, lita_handler: true do

  # As a dev I want to add a new Story to the Backlog
  it { is_expected.to route_command("add story XYZ").to(:add_story) }

  describe "#add_story" do

    it "Add a new story to the backlog" do
      @rally = double("rally")
      expect(@rally).to receive("create").with("story", {"Name" => "XYZ"}).and_return({"FormattedID" => "US123", "Name" => "YXZ"})

    	send_command("add story XYZ")
      expect(replies.last).to match(/Done, US\d{1,} : XYZ/)
    end

  end
end
