require 'rest-client'
require 'json'
require 'rally_api'

module Lita
  module Handlers
    class KanbanRally < Handler

      config :base_url, type: String, required: true
      config :version, type: String, default: 'v2.0'
      config :workspace, type: String, required: true
      config :project, type: String, required: true
      config :username, type: String
      config :password, type: String
      config :api_key, type: String

      route(/^add story\s+(.+)/, :add_story, command: true, help: {
        "add story <name of story>" => "Add a User Story to the backlog"
      })


      def add_story(response)

        fields = {}
        fields["Name"] = response.matches[0][0].to_s

        new_story = rally.create("story", fields)

        story_id = new_story["FormattedID"]
        story_name = new_story["Name"]

        response.reply("Done, "+ story_id.to_s + " : " + story_name)

      rescue Exception => ouch
        puts "Rescued #{ouch.class}"
        puts "Error Message: #{ouch}"
      end

      def rally

        @rally if instance_variable_defined?('@rally')

        rally_config = {
            base_url: config.base_url,
            api_version: config.version,
            workspace: config.workspace,
            project: config.project,
            username: config.username,
            password: config.password,
            api_key: config.api_key
          }

        @rally = RallyAPI::RallyRestJson.new(rally_config)

      end

      Lita.register_handler(KanbanRally)
    end
  end
end
