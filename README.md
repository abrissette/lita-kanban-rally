# lita-kanban-rally

A basic handler for [Lita](http://lita.io/) that integrate with a Kanban board in Rally.

## Installation

Add lita-kanban-rally to your Lita instance's Gemfile:

``` ruby
gem "lita-kanban-rally"
```

## Usage

```
 [You] Lita: add story <DESCRIPTION>
[Lita] Done, US123 : <DESCRIPTION>
```

```
 [You] Lita: add defect <DESCRIPTION>
[Lita] Done, DE123 : <DESCRIPTION>
```

## License

[MIT](http://opensource.org/licenses/MIT)